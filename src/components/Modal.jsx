import { useCallback, useEffect, useRef } from 'react';
import styles from './styles/Modal.module.scss';
import PropTypes from 'prop-types';


function Modal({ header, closeButton, text, actions, showModal, setShowModal }) {

    const modalRef = useRef();

    const closeModal = e => {
        if (modalRef.current === e.target) {
            setShowModal(false)
        }
    };

    const keyPress = useCallback(
        e => {
            if (e.key === "Escape" && showModal) {
                setShowModal(false);
                console.log('I pressed ECS')
            }
        },
        [setShowModal, showModal]
    );

    useEffect(
        () => {
            document.addEventListener('keydown', keyPress);
            return () => document.removeEventListener('keydown', keyPress)
        },
        [keyPress]
    );

    return (
        <>
            {showModal ? (
                <div onClick={closeModal} ref={modalRef}>
                    <div
                        className={styles.modal}

                    >
                        <div className={styles.nav}>
                            <h2 className={styles.navTitle}>{header}</h2>
                            <p className={styles.navClose} onClick={closeButton}>x</p>
                        </div>
                        <p className={styles.modalText}>{text}</p>
                        <div className={styles.modalButtonBlock}>
                            {actions.map((button, index) => (
                                <div key={index}>{button}</div>
                            ))}
                        </div>

                    </div>
                </div>
            ) : null}
        </>
    )
}

Modal.propTypes= {
    header: PropTypes.string.isRequired,
    closeButton: PropTypes.func.isRequired,
    text: PropTypes.string.isRequired, 
    actions: PropTypes.array.isRequired, 
    showModal: PropTypes.bool.isRequired, 
    setShowModal: PropTypes.func.isRequired
  }



export default Modal