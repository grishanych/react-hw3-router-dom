
import React, {useEffect, useState} from 'react';
import styles from './styles/ProductsInCart.module.scss'

function Favorites(props) {

const [selectedProducts, setSelectedProducts] = useState([]);

useEffect(() => {
  if (props.selectedProducts) {
    setSelectedProducts(props.selectedProducts);
  }
}, [props.selectedProducts]);

  return (
    <div >
      <h2>Favorites:</h2>
      <ul className={styles.card}>
        {props.selectedProducts.map((product) => (
          <li className={styles.productCard} key={product.id}>
            <h3> {product.brandName}</h3>
            <p>{product.price} $</p>
          <img className={styles.selectedImg} src={product.image} alt="" />
          {/* <button onClick={() => props.deleteHandler(product.id)}>Delete Card</button>  */}
          </li>
        ))}
      </ul>
     
    </div>
  );
}


export default Favorites